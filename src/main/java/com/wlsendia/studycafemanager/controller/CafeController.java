package com.wlsendia.studycafemanager.controller;

import com.wlsendia.studycafemanager.model.CafeMembershipRegistrationRequest;
import com.wlsendia.studycafemanager.service.CafeCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CafeController {
    private CafeCustomerService service;

    public String setCustomer(@RequestBody @Valid CafeMembershipRegistrationRequest request){
        service.setCustomer(request);

        return "1";
    }

    @PutMapping("/info/id/{id}")
    public String putCustomer(@RequestBody @Valid CafeMembershipRegistrationRequest request, @PathVariable long id){
        service.putCustomer(request, id);

        return "2";
    }

}
