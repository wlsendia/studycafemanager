package com.wlsendia.studycafemanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class CafeCustomer {


    //유저 정보
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String customerId;

    @Column(nullable = false, length = 24)
    private String customerPassword;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false, length = 16)
    private Integer customerPhone;

    //

    private Integer seatNumber;

    private Integer seatRemainTime;

    //private Integer additionalOrder;

    private LocalDate joinDate;

    private LocalDateTime entranceTime;

    private LocalDateTime willLeaveTime;

}
