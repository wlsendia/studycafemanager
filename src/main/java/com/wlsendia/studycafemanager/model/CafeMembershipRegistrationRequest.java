package com.wlsendia.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CafeMembershipRegistrationRequest {
    private String customerId;

    private String customerPassword;

    private String customerName;

    private Integer customerPhone;
}
