package com.wlsendia.studycafemanager.repository;

import com.wlsendia.studycafemanager.entity.CafeCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CafeCustomerRepository extends JpaRepository<CafeCustomer, Long> {
}
