package com.wlsendia.studycafemanager.service;

import com.wlsendia.studycafemanager.entity.CafeCustomer;
import com.wlsendia.studycafemanager.model.CafeMembershipRegistrationRequest;
import com.wlsendia.studycafemanager.repository.CafeCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CafeCustomerService {

    private final CafeCustomerRepository repository;

    public void setCustomer(CafeMembershipRegistrationRequest request)/*회원가입*/{
        CafeCustomer addData = new CafeCustomer();

        addData.setCustomerId(request.getCustomerId());
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPassword(request.getCustomerPassword());
        addData.setCustomerPhone(request.getCustomerPhone());

        repository.save(addData);
    }

    public void putCustomer(CafeMembershipRegistrationRequest request, long id){
        CafeCustomer origin = repository.findById(id).orElseThrow();

        //origin.set


        CafeCustomer addData = new CafeCustomer();



    }
}
